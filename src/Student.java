public class Student {

    private String name;
    private char gender;
    private double gpa;
    private double grade;
    private double age;

    public Student(String name, char gender, double gpa, double grade, double age)
    {

        this.name=name;
        this.gender=gender;
        this.gpa=gpa;
        this.grade=grade;
        this.age=age;

    }

    public String getName(){return this.name;}
    public void setName(String name){this.name=name;}

    public char getGender(){return this.gender;}
    public void setGender(char gender){this.gender=gender;}

    public double getGpa(){return this.gpa;}
    public void setGpa(double gpa){this.gpa=gpa;}

    public double getGrade(){return this.grade;}
    public void setGrade(double grade){this.grade=grade;}

    public double getAge(){return this.age;}
    public void setAge(double age){this.age=age;}


}
