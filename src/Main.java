import java.util.ArrayList;

public class Main {


    public static void main(String[] args) {
        example();

        boolean output1;
        output1 = isOdd(7);
        System.out.println(output1);

        double output2;
        output2 = add(1,2);
        System.out.println(output2);

        int output3;
        output3 = subtract(1,2);
        System.out.println(output3);

        int output4;
        output4 = multiply(5,2);
        System.out.println(output4);

        float output5;
        output5 = divide(5,2);
        System.out.println(output5);

        float output6;
        output6 = mod(5,2);
        System.out.println(output6);

        String output7;
        output7 = name("Jeff");
        System.out.println(output7);

        boolean output8;
        output8 = isPositive(7);
        System.out.println(output8);

        boolean output9;
        output9 = matchedStrings("Joe","Joe");
        System.out.println(output9);

        double output10;
        output10 = higher(2,8);
        System.out.println(output10);

        double output11;
        output11 = absolute(-6.76);
        System.out.println(output11);

        double output12;
        output12 = generate(0, 100);
        System.out.println(output12);

        Student A = new Student("Jeff", 'M', 3.2, 7, 12);
        Student B = new Student("Bob", 'M', 3.9, 3, 7);
        Student C = new Student("Marry", 'F', 3.1, 6, 11);


        Student[] Students = new Student[3];
        Students[0] = A;
        Students[1] = B;
        Students[2] = C;

        System.out.println(Students[1].getName());

        ArrayList<Student>StudentList = new ArrayList<Student>();
        StudentList.add(A);
        StudentList.add(B);
        StudentList.add(C);

        System.out.println(StudentList.get(2).getName());

        for(int i=0; i<2; i++){
            System.out.println(Students[i].getName());
            System.out.println(Students[i].getGrade());
        }

        for(Student student:Students){
            System.out.println(student.getName());
            System.out.println(student.getGpa());
        }

        boolean two = false;
        double iteration = 0;
        double num = 2.0;
        while( !two){
            num = num * 2.0;
            iteration++;
            if (num == 2147483648.0){two = true;}
        }
        System.out.println(iteration);

        double i = 0;
        double n = 2.0;

        do {
            n = n * 2.0;
            i++;

            }
        while(n<2147483648.0);
        System.out.println(n + " " + i);


        char letter = 'c';
        boolean vowel;

        switch (letter){
            case 'a': vowel=true;
                break;
            case 'e': vowel=true;
                break;
            case 'i': vowel=true;
                break;
            case 'o': vowel=true;
                break;
            case 'u': vowel=true;
                break;
            default: vowel=false;
                break;
        }
        System.out.println(vowel);

        boolean vowl;
        if(letter == 'a'){
            vowl = true;
        }else if(letter == 'e'){
            vowl = true;
        }else if(letter == 'i'){
            vowl = true;
        }else if(letter == 'o'){
            vowl = true;
        }else if(letter == 'u'){
            vowl = true;
        }else {
            vowl = false;
        }
        System.out.println(vowel);

        boolean grtzero;
        ArrayList<String> list = new ArrayList <String>();
        list.add("a");
        list.add("d");
        grtzero = grt(list);
        System.out.println(grtzero);

        boolean wordlength;
        wordlength = word("Jefffff");
        System.out.println(wordlength);
    }
    //sout hit tab

    public static void example() {
        System.out.println("Call a function like this");
    }

    public static boolean isOdd(int input) {
        if (input % 2 == 0)
            return false;
        else
            return true;
    }

    public static double add(double input1, double input2) {
        return input1 + input2;
    }

    public static int subtract(int input1, int input2) {
        return input1 - input2;
    }

    public static int multiply(int input1, int input2) {
        return input1 * input2;
    }

    public static float divide(float input1, float input2) {
        return input1 / input2;
    }

    public static float mod(float input1, float input2) {
        return input1 % input2;
    }

    public static String name(String input) {
        return "Hello " + input;
    }

    public static boolean isPositive(int input) {
        if (input > 0)
            return true;
        else
            return false;
    }

    public static boolean matchedStrings(String input1, String input2) {
        if (input1.equals(input2))
            return true;
        else
            return false;
    }

    public static double higher(double input1, double input2) {
        return Math.max(input1, input2);
    }

    public static double absolute(double input) {
        return Math.abs(input);
    }

    public static double generate(double input1, double input2) {
        return (int) (Math.random()*(input2 - input1)) + input1;
    }

    public static boolean grt(ArrayList list) {
        if (list.size()>0)
            return true;
        else
            return false;
    }

    public static boolean word(String Jeff) {
        if (Jeff.length()>5)
            return true;
        else
            return false;
    }
}